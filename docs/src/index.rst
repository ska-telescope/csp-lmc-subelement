.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

SKA CSP.LMC SubElement Base Classes
=======================================

This project provides a limited set of base classes to be extended by each CSP.LMC
SubElement class. 

The scope of the CSP.LMC SubElement Base classes is to provide to a CSP.LMC Element 
a common set of TANGO methods and attributes to access its SubElements in an uniform
way.

.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Introduction 

   README

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: CSP.LMC SubElement TANGO Classes 
  :hidden:

  package/guide


Detailed Class Documentation
=====================================


We report here the detailed descriptions of the component that form part of the project.
This project includes few TANGO Device Classes: the `CspSubElementMaster`, the `CspSubElementSubarray`.


- :doc:`package/guide`
