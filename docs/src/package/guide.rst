.. doctest-skip-all
.. _package-guide:

************************
Public API documentation
************************


.. Automatic API Documentation section. Generating the API from the docstrings. Modify / change the directory structure as you like

CspSubElementMaster TANGO Class
--------------------------------
.. automodule:: cspse.lmc.subelement_master
    :members:
    :member-order:

CspSubElementSubarray TANGO Class
---------------------------------
.. automodule:: cspse.lmc.subelement_subarray
    :members:
    :member-order:
