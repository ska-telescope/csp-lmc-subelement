--extra-index-url https://nexus.engageska-portugal.pt/repository/pypi/simple
docutils
markupsafe
pygments
pylint
pytest
pytest-cov
pytest-pylint
python-dotenv>=0.5.1
setuptools
sphinx
sphinx_rtd_theme
sphinx-autobuild
sphinx-rtd-theme
sphinxcontrib-websupport
pipdeptree
pylint_junit
csp-lmc-common < 0.6.0
