# -*- coding: utf-8 -*-
#
# This file is part of the CspSubElementMaster project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """csp-lmc-subelement"""
version = "0.1.2"
version_info = version.split(".")
description = """SKA CSP Sub-element LMC"""
author = "INAF-OAA"
author_email = "elisabetta.giani@inaf.it"
license = """BSD-3-Clause"""
url = """https://gitlab.com/ska-telescope/csp-lmc.git"""
copyright = """INAF, SKA Telescope"""
