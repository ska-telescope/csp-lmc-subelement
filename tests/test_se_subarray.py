#
# -*- coding: utf-8 -*-
#
# This file is part of the CspSubElementSubarray project
#
"""Contain the tests for the Master."""

# Standard imports
import sys
import os
import time

# Imports
import re
import pytest
from tango import DevState
from tango import DevFailed

# PROTECTED REGION ID(CspSubElementSubarray.test_additional_imports) ENABLED START #
from ska.base.control_model import AdminMode, ControlMode, HealthState, SimulationMode, TestMode
# PROTECTED REGION END #    //  CspSubElementSubarray.test_additional_imports
# Device test case
# PROTECTED REGION ID(CspSubElementSubarray.test_CspSubElementSubarray_decorators) ENABLED START #
@pytest.mark.usefixtures("tango_context")
# PROTECTED REGION END #    //  CspSubElementSubarray.test_CspSubElementSubarray_decorators
class TestCspSubElementSubarray(object):
    """Test case for packet generation."""

    properties = {
        'SkaLevel': '2',
        'LoggingTargetsDefault': '',
        'GroupDefinitions': '',
        }

    @classmethod
    def mocking(cls):
        """Mock external libraries."""
        # Example : Mock numpy
        # cls.numpy = CspSubElementSubarray.numpy = MagicMock()
        # PROTECTED REGION ID(CspSubElementSubarray.test_mocking) ENABLED START #
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_mocking

    def test_properties(self, tango_context):
        # Test the properties
        # PROTECTED REGION ID(CspSubElementSubarray.test_properties) ENABLED START #
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_properties
        pass

    def test_State(self, tango_context):
        """Test for State"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_State) ENABLED START #
        assert tango_context.device.State() == DevState.INIT
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_State

    def test_Status(self, tango_context):
        """Test for Status"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_Status) ENABLED START #
        assert tango_context.device.Status() == "The device is in INIT state."
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_Status

    def test_GetVersionInfo(self, tango_context):
        """Test for GetVersionInfo"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_GetVersionInfo) ENABLED START #
        versionPattern = re.compile(
            r'CspSubElementSubarray, lmcbaseclasses, [0-9].[0-9].[0-9], '
            r'A set of generic base devices for SKA Telescope.')
        versionInfo = tango_context.device.GetVersionInfo()
        assert (re.match(versionPattern, versionInfo[0])) != None
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_GetVersionInfo

    def test_buildState(self, tango_context):
        """Test for buildState"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_buildState) ENABLED START #
        buildPattern = re.compile(
            r'lmcbaseclasses, [0-9].[0-9].[0-9], '
            r'A set of generic base devices for SKA Telescope')
        assert (re.match(buildPattern, tango_context.device.buildState)) != None
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_buildState

    def test_versionId(self, tango_context):
        """Test for versionId"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_versionId) ENABLED START #
        versionIdPattern = re.compile(r'[0-9].[0-9].[0-9]')
        assert (re.match(versionIdPattern, tango_context.device.versionId)) != None
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_versionId

    def test_healthState(self, tango_context):
        """Test for healthState"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_healthState) ENABLED START #
        assert tango_context.device.healthState == HealthState.OK
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_healthState

    def test_adminMode(self, tango_context):
        """Test for adminMode"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_adminMode) ENABLED START #
        tango_context.device.adminMode == AdminMode.ONLINE
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_adminMode

    def test_controlMode(self, tango_context):
        """Test for controlMode"""
        # PROTECTED REGION ID(CspSubElementSubarray.test_controlMode) ENABLED START #
        assert tango_context.device.controlMode == ControlMode.REMOTE
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_controlMode
    
    def test_configureScan_invalid_adminMode(self, tango_context):
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin = ''
            tango_context.device.ConfigureScan(argin)
        assert "Command ConfigureScan can't" in str(df.value.args[0].desc)

    def test_configureScan_invalid_state(self, tango_context):
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            argin = ''
            tango_context.device.ConfigureScan(argin)
        assert "Command ConfigureScan can't" in str(df.value.args[0].desc)

    def test_goToIdle_invalid_adminMode(self, tango_context):
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin = ''
            tango_context.device.GoToIdle()
        assert "Command GoToIdle can't" in str(df.value.args[0].desc)

    def test_goToIdle_invalid_state(self, tango_context):
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            argin = ''
            tango_context.device.GoToIdle()
        assert "Command GoToIdle can't" in str(df.value.args[0].desc)

    def test_scan_invalid_adminMode(self, tango_context):
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin = '1'
            tango_context.device.Scan(argin)
        assert "Command Scan can't" in str(df.value.args[0].desc)

    def test_scan_invalid_state(self, tango_context):
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            argin = '1'
            tango_context.device.Scan(argin)
        assert "Command Scan can't" in str(df.value.args[0].desc)

    def test_endscan_invalid_adminMode(self, tango_context):
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            tango_context.device.EndScan()
        assert "Command EndScan can't" in str(df.value.args[0].desc)

    def test_endscan_invalid_state(self, tango_context):
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            tango_context.device.EndScan()
        assert "Command EndScan can't" in str(df.value.args[0].desc)

    def test_num_of_dev_completed_task(self, tango_context):
        # PROTECTED REGION ID(CspSubElementSubarray.test_num_of_dev_completed_task) ENABLED START #
        """Test numOfCompletedTask attribute"""
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_num_of_dev_completed_task
        assert tango_context.device.numOfDevCompletedTask == None

    def test_list_of_completed_task(self, tango_context):
        # PROTECTED REGION ID(CspSubElementSubarray.test_list_of_completed_task) ENABLED START #
        """Test listOfCompletedTask attribute"""
        assert tango_context.device.listOfDevCompletedTask == None
        # PROTECTED REGION END #    //  CspSubElementSubarray.test_list_of_completed_task

    def test_command_progress(self, tango_context):
        """Test xxxCommandProgress attributes"""
        assert tango_context.device.goToIdleCmdProgress == 0
        assert tango_context.device.configurationProgress == 0

    def test_command_duration_measured(self, tango_context):
        """Test xxxCmdDurationMeasured attributes"""
        assert tango_context.device.goToIdleDurationMeasured == 0
        assert tango_context.device.configureScanDurationMeasured == 0

    def test_command_duration_expected(self, tango_context):
        """Test xxxCmdDurationExpected attributes"""
        assert tango_context.device.goToIdleDurationExpected == 30
        assert tango_context.device.configurationDelayExpected == 30

    def test_set_command_duration_expected(self, tango_context):
        """Test xxxCmdDurationExpected attributes"""
        tango_context.device.goToIdleDurationExpected = 20
        assert tango_context.device.goToIdleDurationExpected == 20


