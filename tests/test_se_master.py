#
# -*- coding: utf-8 -*-
#
# This file is part of the CspSubElementMaster project
#
"""Contain the tests for the Master."""

# Standard imports
import sys
import os
import time

# Imports
import re
import pytest
from tango import DevState
from tango import DevFailed

# PROTECTED REGION ID(CspSubElementMaster.test_additional_imports) ENABLED START #
from ska.base.control_model import AdminMode, ControlMode, HealthState, SimulationMode, TestMode
# PROTECTED REGION END #    //  CspSubElementMaster.test_additional_imports
# Device test case
# PROTECTED REGION ID(CspSubElementMaster.test_CspSubElementMaster_decorators) ENABLED START #
@pytest.mark.usefixtures("tango_context")
# PROTECTED REGION END #    //  CspSubElementMaster.test_CspSubElementMaster_decorators
class TestCspSubElementMaster(object):
    """Test case for packet generation."""

    capabilities = ['FSP:27', 'VCC:197']
    properties = {
        'SkaLevel': '4',
        'LoggingTargetsDefault': '',
        'GroupDefinitions': '',
        'NrSubarrays': '16',
        'CapabilityTypes': '',
        'MaxCapabilities': ['FSP:27', 'VCC:197']
        }

    @classmethod
    def mocking(cls):
        """Mock external libraries."""
        # Example : Mock numpy
        # cls.numpy = CspSubElementMaster.numpy = MagicMock()
        # PROTECTED REGION ID(CspSubElementMaster.test_mocking) ENABLED START #
        # PROTECTED REGION END #    //  CspSubElementMaster.test_mocking

    def test_properties(self, tango_context):
        # Test the properties
        # PROTECTED REGION ID(CspSubElementMaster.test_properties) ENABLED START #
        # PROTECTED REGION END #    //  CspSubElementMaster.test_properties
        pass

    # PROTECTED REGION ID(CspSubElementMaster.test_State_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_State_decorators
    def test_State(self, tango_context):
        """Test for State"""
        # PROTECTED REGION ID(CspSubElementMaster.test_State) ENABLED START #
        assert tango_context.device.State() == DevState.INIT
        # PROTECTED REGION END #    //  CspSubElementMaster.test_State

    # PROTECTED REGION ID(CspSubElementMaster.test_Status_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_Status_decorators
    def test_Status(self, tango_context):
        """Test for Status"""
        # PROTECTED REGION ID(CspSubElementMaster.test_Status) ENABLED START #
        assert tango_context.device.Status() == "The device is in INIT state."
        # PROTECTED REGION END #    //  CspSubElementMaster.test_Status

    # PROTECTED REGION ID(CspSubElementMaster.test_GetVersionInfo_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_GetVersionInfo_decorators
    def test_GetVersionInfo(self, tango_context):
        """Test for GetVersionInfo"""
        # PROTECTED REGION ID(CspSubElementMaster.test_GetVersionInfo) ENABLED START #
        versionPattern = re.compile(
            r'CspSubElementMaster, lmcbaseclasses, [0-9].[0-9].[0-9], '
            r'A set of generic base devices for SKA Telescope.')
        versionInfo = tango_context.device.GetVersionInfo()
        assert (re.match(versionPattern, versionInfo[0])) != None
        # PROTECTED REGION END #    //  CspSubElementMaster.test_GetVersionInfo


    # PROTECTED REGION ID(CspSubElementMaster.test_buildState_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_buildState_decorators
    def test_buildState(self, tango_context):
        """Test for buildState"""
        # PROTECTED REGION ID(CspSubElementMaster.test_buildState) ENABLED START #
        buildPattern = re.compile(
            r'lmcbaseclasses, [0-9].[0-9].[0-9], '
            r'A set of generic base devices for SKA Telescope')
        assert (re.match(buildPattern, tango_context.device.buildState)) != None
        # PROTECTED REGION END #    //  CspSubElementMaster.test_buildState

    # PROTECTED REGION ID(CspSubElementMaster.test_versionId_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_versionId_decorators
    def test_versionId(self, tango_context):
        """Test for versionId"""
        # PROTECTED REGION ID(CspSubElementMaster.test_versionId) ENABLED START #
        versionIdPattern = re.compile(r'[0-9].[0-9].[0-9]')
        assert (re.match(versionIdPattern, tango_context.device.versionId)) != None
        # PROTECTED REGION END #    //  CspSubElementMaster.test_versionId

    # PROTECTED REGION ID(CspSubElementMaster.test_healthState_decorators) ENABLED START #
    # PROTECTED REGION END #    //  CspSubElementMaster.test_healthState_decorators
    def test_healthState(self, tango_context):
        """Test for healthState"""
        # PROTECTED REGION ID(CspSubElementMaster.test_healthState) ENABLED START #
        assert tango_context.device.healthState == HealthState.OK
        # PROTECTED REGION END #    //  CspSubElementMaster.test_healthState

    def test_adminMode(self, tango_context):
        """Test for adminMode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_set_adminMode_to_offline) ENABLED START #
        assert tango_context.device.adminMode == AdminMode.ONLINE
        # PROTECTED REGION END #    //  CspSubElementMaster.test_set_adminMode_to_offline

    def test_On_invalid_adminMode(self, tango_context):
        """Test On command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_On_invalid_adminMode) ENABLED START #
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin  = [""]
            tango_context.device.On(argin)
        assert "On command can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_On_invalid_adminMode

    def test_Off_invalid_adminMode(self, tango_context):
        """Test Off command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_Off_invalid_adminMode) ENABLED START #
        # set the adminMode to OFFLINE to test the behavior of the AdminModeCheck decorator 
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin  = [""]
            tango_context.device.Off(argin)
        assert "Off command can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_Off_invalid_adminMode

    def test_Standby_invalid_adminMode(self, tango_context):
        """Test Standby command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_Standby_invalid_adminMode) ENABLED START #
        # set the adminMode to OFFLINE to test the behavior of the AdminModeCheck decorator 
        tango_context.device.adminMode = AdminMode.OFFLINE
        assert tango_context.device.adminMode == AdminMode.OFFLINE
        with pytest.raises(DevFailed) as df:
            argin  = [""]
            tango_context.device.Standby(argin)
        assert "Standby command can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_Standby_invalid_adminMode

    def test_On_invalid_State(self, tango_context):
        """Test On command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_On_invalid_State) ENABLED START #
        # set the adminMode to ONLINE
        # reinitialize the device to return to
        # adminMode =ONLINE and State=INIT
        tango_context.device.Init()
        assert tango_context.device.adminMode == AdminMode.ONLINE
        assert tango_context.device.State() == DevState.INIT
        with pytest.raises(DevFailed) as df:
            argin = []
            tango_context.device.On(argin)
        assert "Command On can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_On_invalid_State

    def test_Off_invalid_State(self, tango_context):
        """Test Off command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_Off_invalid_State) ENABLED START #
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            argin = []
            tango_context.device.Off(argin)
        assert "Command Off can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_Off_invalid_State

    def test_Standby_invalid_State(self, tango_context):
        """Test Standby command with offline adminmode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_Standby_invalid_State) ENABLED START #
        assert tango_context.device.adminMode == AdminMode.ONLINE
        with pytest.raises(DevFailed) as df:
            argin = []
            tango_context.device.Standby(argin)
        assert "Command Standby can't" in str(df.value.args[0].desc)
        # PROTECTED REGION END #    //  CspSubElementMaster.test_Standby_invalid_State

    def test_controlMode(self, tango_context):
        """Test for controlMode"""
        # PROTECTED REGION ID(CspSubElementMaster.test_controlMode) ENABLED START #
        assert tango_context.device.controlMode == ControlMode.REMOTE
        # PROTECTED REGION END #    //  CspSubElementMaster.test_controlMode

    def test_maxCapabilities(self, tango_context):
        """Test for maxCapabilities"""
        # PROTECTED REGION ID(CspSubElementMaster.test_maxCapabilities) ENABLED START #
        assert tango_context.device.maxCapabilities == ('FSP:27', 'VCC:197')
        # PROTECTED REGION END #    //  CspSubElementMaster.test_maxCapabilities

    def test_availableCapabilities(self, tango_context):
        """Test for availableCapabilities"""
        # PROTECTED REGION ID(CspSubElementMaster.test_availableCapabilities) ENABLED START #
        assert tango_context.device.availableCapabilities == ('FSP:27', 'VCC:197')
        # PROTECTED REGION END #    //  CspSubElementMaster.test_availableCapabilities

    def test_num_of_dev_completed_task(self, tango_context):
        """Test numOfCompletedTask attribute"""
        assert tango_context.device.numOfDevCompletedTask == 0

    def test_list_of_completed_task(self, tango_context):
        """Test listOfCompletedTask attribute"""
        assert tango_context.device.listOfDevCompletedTask == None

    def test_list_of_components(self, tango_context):
        """Test listOfComponents attribute"""
        assert tango_context.device.listOfComponents == None

    def test_timeout_expired(self, tango_context):
        """Test xxxTimeoutExpired flags attribute"""
        timeout = tango_context.device.onCmdTimeoutExpired
        #assert not tango_context.device.onCmdTimeoutExpired
        assert timeout == 0
        assert not tango_context.device.offCmdTimeoutExpired
        assert not tango_context.device.standbyCmdTimeoutExpired

    def test_failure_raised(self, tango_context):
        """Test xxxCmdFailure attributes"""
        assert not tango_context.device.onCmdFailure
        assert not tango_context.device.offCmdFailure
        assert not tango_context.device.standbyCmdFailure

    def test_failure_message(self, tango_context):
        """Test xxxFailureMessage attributes"""
        assert not tango_context.device.onFailureMessage
        assert not tango_context.device.offFailureMessage
        assert not tango_context.device.standbyFailureMessage

    def test_command_progress(self, tango_context):
        """Test xxxCommandProgress attributes"""
        assert tango_context.device.onCommandProgress == 0
        assert tango_context.device.offCommandProgress == 0
        assert tango_context.device.standbyCommandProgress == 0

    def test_command_duration_measured(self, tango_context):
        """Test xxxCmdDurationMeasured attributes"""
        assert tango_context.device.onCmdDurationMeasured == 0
        assert tango_context.device.offCmdDurationMeasured == 0
        assert tango_context.device.standbyCmdDurationMeasured == 0

    def test_command_duration_expected(self, tango_context):
        """Test xxxCmdDurationExpected attributes"""
        assert tango_context.device.onCmdDurationExpected == 30
        assert tango_context.device.offCmdDurationExpected == 30
        assert tango_context.device.standbyCmdDurationExpected == 30

    def test_set_command_duration_expected(self, tango_context):
        """Test xxxCmdDurationExpected attributes"""
        tango_context.device.onCmdDurationExpected = 20
        tango_context.device.offCmdDurationExpected = 20
        tango_context.device.standbyCmdDurationExpected = 20
        assert tango_context.device.onCmdDurationExpected == 20
        assert tango_context.device.offCmdDurationExpected == 20
        assert tango_context.device.standbyCmdDurationExpected == 20


