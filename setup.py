#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from setuptools import setup, find_packages

setup_dir = os.path.dirname(os.path.abspath(__file__))

# make sure we use latest info from local code
sys.path.insert(0, setup_dir)

INFO = {}

with open('README.md') as readme_file:
    long_description = readme_file.read()

RELEASE_FILENAME = os.path.join(setup_dir, 'cspse','lmc','release.py')
exec(open(RELEASE_FILENAME).read(), INFO)
setup(
    name=INFO['name'],
    version=INFO['version'],
    description=INFO['description'],
    author=INFO['author'],
    author_email=INFO['author_email'],
    packages=find_packages(),
    license=INFO['license'],
    url=INFO['url'],
    long_description=long_description,
    keywords="csp lmc ska tango",
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Operating System :: POSIX :: Linux',
        'License :: Other/Proprietary License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    test_suite='tests',
    install_requires=[
        'pytango >=9.3.1',
        'future',
        'csp-lmc-common < 0.6.0',
    ],  
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
        'recommonmark'
    ],
    tests_require=[
        'pytest',
        'coverage',
        'pytest-json-report',
        'pytest-forked',
        'pycodestyle',
        'mock'
    ],
    entry_points={
        "console_scripts": [
            "CspSubElementMaster=cspse.lmc.subelement_master:main",
            "CspSubElementSubarray=cspse.lmc.subelement_subarray:main",
        ]
    }
)
