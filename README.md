
SKA CSP-LMC-SUBELEMENT Abstract Class
=====================================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-csp-lmc-subelement/badge/?version=latest)](https://developer.skatelescope.org/projects/csp-lmc-subelement/en/latest/?badge=latest)

## Table of contents
* [Description](#description)
* [Getting started](#getting-started)
* [Repository](#repository)
* [Prerequisities](#prerequisities)
* [Running tests](#running-tests)
* [Known bugs](#known-bugs)
* [Troubleshooting](#troubleshooting)
* [License](#license)

## Description

This project contains the `CSP.LMC.SUBELEMENT` prototype. It includes a
single class:

* the `CspSubElementMaster` device: based on the `CspMaster` class. The 
`CspSubElementMaster` represents a primary point of contact for CSP
SubElement Monitor and Control.  It implements CSP SubElement state and
mode indicators and a limited set of housekeeping commands.
It is intended to connect to the various subcomponent of SubElement. This
can be accomplished directly or by means of a _caching_ device, called
`Rack`. Of course it is a device collector and can or cannot correspond to a
physical rack.


## Getting started

The project can be found in the SKA gitlab repository.

To get a local copy of the project:

```bash
git clone https://gitlab.com/ska-telescope/csp-lmc-subelement.git
```
## Prerequisities

* A TANGO development environment properly configured, as described in [SKA developer portal](https://developer.skatelescope.org/en/latest/tools/tango-devenv-setup.html)

* [SKA Base classes](https://gitlab.com/ska-telescope/lmc-base-classes)


## Repository organization

The `CSP.LMC.SUBELEMENT` repository is organized in a single code tree. The
hierarchy contains:

* _cspse_: contains the specific project TANGO Device Class files
* _pogo_: contains the POGO files of the TANGO Device Classes of the project
* _docker_: contains the `docker`, `docker-compose` and `dsconfig` configuration files as well as the Makefile to generate the docker image and run the tests.
* _tests_: contains the test

## Running tests

To run the internal test go to `tests` directory and execute:

```bash
make test
```

## Known bugs

_Still none_

## Troubleshooting

TBD

## License
See the LICENSE file for details.


